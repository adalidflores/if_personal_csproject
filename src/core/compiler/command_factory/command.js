/*
@command.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const CompilersServiceError = require('../../../common/errors/compilers_service_error');


// Builds the Command class
class Command {

    // Defines the constructor for its children
    constructor() {
        if (this.constructor == Command) {
          throw new CompilersServiceError("Abstract classes can't be instantiated.");
        }
    }
  
    // Abstract method for its children that returns an array with the necessary commands to project run.
    builder(parameters) {
        throw new CompilersServiceError("Method 'builder()' must be implemented.");
    }
}

// Exports Command class
module.exports = Command
