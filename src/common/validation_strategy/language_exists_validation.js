/*
 @exists_language_validation.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const IValidator = require("./i_validator");
const constants = require('../constants/constants')
const CompilersServiceError = require('./../errors/compilers_service_error');


// builds LanguageExistsValidation class
class LanguageExistsValidation extends IValidator {
    
    // Defines the constructor
    constructor(language) {
        super();
        this.language = language;
    }

    // Validates if the language exists
    validate() {
        if(constants.languages[this.language] == null){
            throw new CompilersServiceError("this language does not exist");
        }
    }
}

// Exports LanguageExistsValidation class
module.exports = LanguageExistsValidation;
