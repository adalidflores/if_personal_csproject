/*
 @file_routes.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict'

const express = require('express');
const api = express.Router();
const FileController = require('./../controllers/file_controller');
const file_controller = new FileController();

// File methods
api.post('/file', file_controller.save_file);
api.put('/file/:id', file_controller.update_file);
api.delete('/file/:id', file_controller.delete_file);
api.get('/file/:id', file_controller.get_project_files_by_id);

// Exports routes
module.exports = api;
