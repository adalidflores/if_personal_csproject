/*
 @console_controller.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


'use strict'

const project_model = require('../models/project_model');
const constants = require('./../common/constants/constants');
const Facade = require('../core/compiler/command_factory/command_factory_facade');
const Results = require('../core/results');


class ConsoleController {

    // Gets project console result
    async get_console_result(req, res) {
        try {
            const project_id = req.params.id;
            const project = await project_model.findById(project_id);
            let facade = new Facade();
            const result = await facade.run_project(constants.languages[project.language].path, constants.projects_path
                , project.name, project.language);
            res.json({pid: result.get_pid(), console: result.get_result()});
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "Project doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }
}

// Exports class
module.exports = ConsoleController;
