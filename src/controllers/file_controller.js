/*
 @file_controller.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


'use strict'

const file_model = require('./../models/file_model');
const project_model = require('./../models/project_model');
const Utils = require('./../common/utils/utils');
const utils = new Utils();
const ValidationFileContoller = require('./../common/utils/validation_file_controller');
const validation_file_controller = new ValidationFileContoller();


class FileController {

    // Saves file
    async save_file(req, res) {
        try {
            validation_file_controller.validate_save_file_data(req.body.file_name, req.body.project_id, req.body.content);
            const project = await project_model.findById(req.body.project_id);
            const data_file = utils.get_data_file(project, req.body);
            validation_file_controller.validate_save_file_path(data_file.path);
            let file = new file_model();
            file.file_name = req.body.file_name;
            file.project = req.body.project_id;
            utils.create_file(data_file);
            file = await file.save();
            res.json(file);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "Project doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }

    // Updates file
    async update_file(req, res) {
        try {
            validation_file_controller.validate_update_file(req.body.content);
            const file_id = req.params.id;
            const content = req.body.content;
            const file = await file_model.findById(file_id);
            const project_id = file.project;
            const project = await project_model.findById(project_id);
            const file_path = utils.get_file_path(project, file);
            utils.create_file({path: file_path, content: content});
            file.content = content;
            res.json(file);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "File doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }

    // Deletes file
    async delete_file(req, res) {
        try {
            const file_id = req.params.id;
            let file = await file_model.findByIdAndRemove(file_id);
            const project = await project_model.findById(file.project);
            const file_path = utils.get_file_path(project, file);
            utils.delete_path(file_path);
            res.json(file);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "File doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }

    // Gets files of a project
    async get_project_files_by_id(req, res) {
        try {
            const project_id = req.params.id;
            const files = await file_model.find({project: project_id});
            res.json(files);
        } catch (err) {
            if(err.kind == 'ObjectId') {
                err.message = "Project doesn't exist in database";
            }
            res.status(400).json({error: err.message});
        }
    }
}

// Exports class
module.exports = FileController;
